let board = [
   [0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0]
]

const limitX = board[0].length - 3;
const limitY = board.length - 3;
const columnOne = document.getElementById("column-1")
const columnTwo = document.getElementById("column-2")
const columnThree = document.getElementById("column-3")
const columnFour = document.getElementById("column-4")
const columnFive = document.getElementById("column-5")
const columnSix = document.getElementById("column-6")
const columnSeven = document.getElementById("column-7")
const result = document.getElementById("result")
const btnStart = document.getElementById("btnStart")
const divStart = document.getElementById("start")
const divGame = document.getElementById("game")
const btnRestart = document.getElementById("btnRestart")
const divRestart = document.getElementById("restart")
const soundMove = document.getElementById("soundMove")
const soundVictory = document.getElementById("soundVictory")
const btnMute = document.getElementById("mute")
const containerPlayer = document.getElementById("containerPlayer")
const pointPlayer = document.getElementById("pointPlayer")
const classColumn = document.querySelectorAll("div.column")
const scoreOfGame = document.getElementById("score")
const scorePlayer1 = document.getElementById("scoreplayer1")
const scorePlayer2 = document.getElementById("scoreplayer2")
const container__score = document.getElementById("container__score")
let player = 1
let message;
let sumPlayerOne = 0
let sumPlayerTwo = 0

btnStart.addEventListener("click", function (event) {
   divGame.style.display = "flex"
   divStart.style.display = "none"
   containerPlayer.style.display = "flex"
   container__score.style.display = "flex"
})

btnRestart.addEventListener("click", function (event) {
   board = [
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0]
   ]
   addEventListener();
   divStart.style.display = "none"
   restart.style.display = "none"
   container__score.style.display = "flex"
   containerPlayer.style.display = "flex"
   for (let i = 0; i < classColumn.length; i++) {
      classColumn[i].innerHTML = ''
   }

   for (let i = 0; i < classColumn.length; i++) {
      classColumn[i].style.zIndex = "initial"
   }
})

btnMute.addEventListener("click", function (event) {
   if (soundMove.muted === false) {
      btnMute.style.backgroundImage = "url('./assets/img/muteBtn.png')"
      soundMove.muted = true
   } else {
      soundMove.muted = false
      btnMute.style.backgroundImage = "url('./assets/img/somBtn.png')"
   }
})

function highlightCells(line, column, direction) {
   switch (direction) {
      case 'horizontal':
         for (let i = 0; i < 4; i++) {
            let cell = classColumn[column++].children[line]
            cell.className = 'scale'
         }
         break;
      case 'vertical':
         for (let i = 0; i < 4; i++) {
            let cell = classColumn[column].children[line++]
            cell.className = 'scale'
         }
         break;
      case 'diagonalRight':
         for (let i = 0; i < 4; i++) {
            let cell = classColumn[column++].children[line++]
            cell.className = 'scale'
         }
         break;
      case 'diagonalLeft':
         for (let i = 0; i < 4; i++) {
            let cell = classColumn[column++].children[line--]
            cell.className = 'scale'
         }
         break;
   }
   removeEventListener()
   setTimeout(() => {
      showWinnerOrDraw()
   }, 500)
}

function showWinnerOrDraw() {
   restart.style.display = "block"
   containerPlayer.style.display = "none"
   if (message === 'victory') {
      let winner = player;
      if (winner === 1) {
         sumPlayerOne++
         scorePlayer1.innerText = `${sumPlayerOne}`
      } else {
         sumPlayerTwo++
         scorePlayer2.innerText = `${sumPlayerTwo}`
      }
      container__score.style.display = "none"
      result.innerText = `Player ${winner} won!!!`;
      soundVictory.play()
   } else {
      result.innerText = `Draw game over`;
   }
   for (let i = 0; i < classColumn.length; i++) {
      classColumn[i].style.zIndex = "-1"
   }
}

function putBall(column) {
   if (column.childElementCount < 6) {
      if (column.id != "player1" && column.id != "player2") {
         if (player === 1) {
            const blackBall = document.createElement("div")
            blackBall.id = "player1"
            column.appendChild(blackBall)
            soundMove.play()
            player = 2
         } else {
            const redBall = document.createElement("div")
            redBall.id = "player2"
            column.appendChild(redBall)
            soundMove.play()
            player = 1
         }
      }
   }
}

function roundPlayer(column) {
   if (player === 1) {
      pointPlayer.style.backgroundColor = "#d7263d"
   } else {
      pointPlayer.style.backgroundColor = "#c5d86d"
   }
}

function countAndPutInArray(column) {
   let row = column.childElementCount
   let columnOfArr = column.id
   let array = board[row]
   if (columnOfArr === "column-1") {
      array[0] = player
   } else if (columnOfArr === "column-2") {
      array[1] = player
   } else if (columnOfArr === "column-3") {
      array[2] = player
   } else if (columnOfArr === "column-4") {
      array[3] = player
   } else if (columnOfArr === "column-5") {
      array[4] = player
   } else if (columnOfArr === "column-6") {
      array[5] = player
   } else if (columnOfArr === "column-7") {
      array[6] = player
   }
}

function horizontalVictory() {
   for (let line = 0; line < board.length; line++) {
      for (let column = 0; column < limitX; column++) {
         const cell = board[line][column];
         if (cell !== 0) {
            if (cell === board[line][column + 1] &&
               cell === board[line][column + 2] &&
               cell === board[line][column + 3]) {
               message = 'victory'
               highlightCells(line, column, 'horizontal');
            }
         }
      }
   }
   return false
}

function verticalVictory() {
   for (let line = 0; line < limitY; line++) {
      for (let column = 0; column < board[line].length; column++) {
         const cell = board[line][column];
         if (cell !== 0) {
            if (cell === board[line + 1][column] &&
               cell === board[line + 2][column] &&
               cell === board[line + 3][column]) {
               message = 'victory'
               highlightCells(line, column, 'vertical')
            }
         }
      }
   }
   return false
}

function diagonalRight() {
   for (let line = 0; line < limitY; line++) {
      for (let column = 0; column < limitX; column++) {
         const cell = board[line][column];
         if (cell !== 0) {
            if (cell === board[line + 1][column + 1] &&
               cell === board[line + 2][column + 2] &&
               cell === board[line + 3][column + 3]) {
               message = 'victory'
               highlightCells(line, column, 'diagonalRight')
            }
         }
      }
   }
   return false
}

function diagonalLeft() {
   for (let line = 3; line < board.length; line++) {
      for (let column = 0; column < limitX; column++) {
         const cell = board[line][column];
         if (cell !== 0) {
            if (cell === board[line - 1][column + 1] &&
               cell === board[line - 2][column + 2] &&
               cell === board[line - 3][column + 3]) {
               message = 'victory'
               highlightCells(line, column, 'diagonalLeft')
            }
         }
      }
   }
   return false
}

function verifyVictory() {
   horizontalVictory()
   verticalVictory()
   diagonalRight()
   diagonalLeft()
}

function verifyFullBoard() {
   for (let line = 0; line < board.length; line++) {
      for (let column = 0; column < board[0].length; column++) {
         const cell = board[line][column];
         if (cell === 0) return false
      }
   }
   message = 'draw'
   showWinnerOrDraw()
}

function capturingColumn(event) {
   let column = event.target
   countAndPutInArray(column)
   putBall(column)
   verifyVictory()
   verifyFullBoard()
   roundPlayer()
}

function addEventListener() {
   classColumn.forEach((column) => {
      column.addEventListener('click', capturingColumn);
   });
}

function removeEventListener() {
   classColumn.forEach((column) => {
      column.removeEventListener('click', capturingColumn);
   });
}

addEventListener();